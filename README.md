# Dockers #

Setup: Ubuntu 18.04 with 8GB RAM

## Docker Labs - 1

1. Update the system

```sudo apt-get update```

2. Install necessary packages

```sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common```

3. Fetch keys to use dockerhub

```curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -```

4. Add docker repo

```sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"```

5. Install docker

```sudo apt-get install docker-ce docker-ce-cli containerd.io```

6. Enable docker

```sudo systemctl enable docker```

7. Configure to start docker at startup

```sudo systemctl start docker```

8. Verify docker version

```sudo docker -v```

9. Add new group

```sudo groupadd docker```

10. Add current user to the group

```sudo usermod -aG docker $USER```

11. Logout and login

12. Run docker hello-world container

```docker run hello-world```

13. Run docker nginx container

```docker run --name nginx-test -d bitnami/nginx:latest```

14. Verify the docker containers running

```docker ps```

15. Stop a container

```docker stop nginx-test```

16. Start a container

```docker start nginx-test```

17. Run a bash terminal on the container

```docker exec -it nginx-test bash```

18. Exit the SSH 

```exit```

19. Check the logs of a container

```docker logs nginx-test```

20. Stop and remove the container

```docker stop nginx-test```

```docker rm nginx-test```

```docker ps```

21. List docker images

```docker image list```

## Dockers Lab - 2

1. Create volume

```docker volume create pv-mysql-data```

2. List volumes

```docker volume list```

3. Run Mysql with volume

```docker run --name mysql-01 -v pv-mysql-data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=my-password -d mysql```

4. Remove container along with volume

```docker rm -v mysql-01```

5. Remove unused volumes

```docker volume prune```

## Dockers Lab - 3

1. List the networks

```docker network list```

2. Create network

```docker network create frontend```

3. Start container on a network

```docker run --network=frontend --name mysql-01 -v pv-mysql-data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=my-password -d mysql```

4. Start container and expose the port on host network

```docker run --network=host --name nginx-test -d bitnami/nginx:latest```

5. Check the port

```curl http://localhost:8080```

```netstat -an | grep 8080```

6. Start container and expose port on bridge network

```docker run -p 8080:8080 --name nginx-test -d bitnami/nginx:latest```

```docker ps```

```curl http://localhost:8080```

```docker run -p 8081:8080 --name nginx-test -d bitnami/nginx:latest```

```docker ps```

```curl http://localhost:8081```

```netstat -an | grep 80```
